Bébés des blés
=============

Ce dépôt contient les données sur les prix du blé et naissances à Uzerche de 1869 à 1881, ainsi que les scripts R qui ont servi à l'analyse des données.

Après avoir copié le dépôt entier, il est possible d'exécuter chaque script de manière indépendante (le plus simple est d'ouvrir `code_folder.Rproj` dans RStudio pour que le working directory soit choisi correctement)

Les données sur les mariages nous ont été fournies par le groupe travaillant sur ce sujet.

#### Structure des dossiers :
- data_dirty : données initiales sur Uzerche (en .xlsx)
- data : données nettoyées et stockées en CSV (généré par le script `clean_data.R`)
  - `registre_full.csv` : toutes les données de naissances
  - `registre.csv` : registre des naissances sans les enfants de père inconnu
  - `mercuriales.csv` : prix du blé (données nettoyées)
  - `data_mois.csv` et `data_year.csv` sont les mêmes données moyennées par mois ou par an
- scripts: tous les programmes R
  - `clean_data.R` : construction des données nettoyées et stockage en CSV (génère les fichiers du dossier `./data` à partir du dossier `./data_dirty`)
  - `cross_correlation.R` : calcul et représentation de la correlation croisée
  - `CSP.R` : régressions sur des sous-ensembles de CSP
  - `descriptive_stats.R` : statistiques descriptives sur les données
  - `granger_causality` : calcul de la causalité entre séries temporelles
  - `plot.R` : visualisation des données
  - `regressions.R` : analyse des données par OLS
- report: le rapport du projet (et les figures produites dans report/Figs)

