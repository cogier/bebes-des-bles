require("tidyverse")
require("jtools")
require("AER")
require("stargazer")
require("plm")
require("readxl")
require("lubridate")

### Importing birth data

registre_full <- read_excel("data_dirty/registre1869.xlsx")

registre_full <- registre_full %>% 
  mutate(
    date_str = paste(annee, mois,jour, sep="-"),
    date = as.Date(date_str),
    jumeaux = !(is.na(jumeaux)),
    en_ville = ville == "uzerche",
    adresse = case_when(
      en_ville ~ adresse,
      TRUE ~ ville),
    enfant_nat = is.na(prof_pere),
    female = sexe == "f",
  ) %>% 
  select(
    date, nom_pere, prof_pere, typo, age_pere, prof_mere, age_mere, female,
    en_ville, adresse, jumeaux, enfant_nat)
  

registre_full = registre_full %>%
  mutate(
    adresse = case_when(
      grepl("lion", adresse, fixed = TRUE) ~ "rue du lion d'or",
      grepl("eulalie", adresse, fixed = TRUE) ~ "faubourg sainte eulalie",
      grepl("pomme", adresse, fixed = TRUE) ~ "faubourg de la pomme",
      grepl("escaliers", adresse, fixed = TRUE) ~ "rue des escaliers notre-dame",
      grepl("notre", adresse, fixed = TRUE) ~ "place notre-dame", # attention prend les escaliers
      grepl("barry", adresse, fixed = TRUE) ~ "rue du barry", # attention, prend aussi impasse du barry
      grepl("lezes", adresse, fixed = TRUE) ~ "chemin des lezes",
      grepl("becharie", adresse, fixed = TRUE) ~ "rue porte becharie",
      TRUE ~ adresse)
  )

write_csv(registre_full, "data/registre_full.csv")

registre = registre_full %>% filter(!enfant_nat)
write_csv(registre, "data/registre.csv")



## Observations on the birth data
registre %>% filter(!en_ville) %>% select(adresse) %>% unique %>% arrange(adresse) %>% as.list
registre %>% filter(!en_ville) %>% select(adresse) %>% unique %>% arrange(adresse) %>% as.list

registre %>% select(prof_mere) %>% unique %>% arrange(prof_mere) %>% as.list
registre %>% select(prof_pere) %>% unique %>% arrange(prof_pere) %>% as.list

# TODO: utiliser un fuzzy matching avec string dist pour nettoyer
registre %>% select(nom_pere) %>% unique %>% arrange(nom_pere) %>% as.list


### Importing the price data :
##### Small sample from Uzerche only :
prix <- 
  read_excel("data_dirty/prix_ble.xlsx") %>% 
  mutate(
    jour = case_when(
      (quinzaine == 1) ~ 1,
      (quinzaine == 2) ~ 15,
      TRUE ~ NA_real_),
    date_str = paste(annee, mois,jour, sep="-"),
    date = as.Date(date_str)
    ) %>% 
  filter(origine_data==2) %>% 
   select(date, qte_hecto_froment, px_hecto_froment) %>% 
  filter( !is.na(px_hecto_froment)) %>% 
  mutate(ville = "uzerche")
    
##### Large sample from Correze :
l_mois = c(
  "janvier","fevrier","mars","avril","mai","juin",
  "juillet","aout","septembre","octobre", "novembre", "decembre"
)

prix_tot <- 
  read_csv2("data_dirty/mercuriales_froment_1847_1906.csv") %>% 
  mutate(
    jour = case_when(
      (quinzaine == 1) ~ 1,
      (quinzaine == 2) ~ 15,
      TRUE ~ NA_real_),
    mois = match(mois, l_mois),
    date_str = paste(annee, mois,jour, sep="-"),
    date = as.Date(date_str),
    px_hecto_froment = px_hecto_froment/100
  ) %>% 
  select(date, ville, qte_hecto_froment, px_hecto_froment) %>% 
  filter( !(ville %in% c("moyenne_annuelle")) ) %>% 
  filter( !(ville %in% c("moyenne")) ) %>% 
  filter( !is.na(px_hecto_froment))

#### combined in one database
prix_tot = rbind(prix, prix_tot)

write_csv(prix_tot, "data/mercuriales.csv")


## Observations on the price database
prix_tot %>%  
  filter(date >= as.Date("1868-01-01") & date <= as.Date("1883-01-01")) %>%  
  group_by(ville) %>%  summarise(n())


ggplot(data = prix_tot, mapping=aes(x = date, color = ville)) + geom_freqpoly()

prix_tot %>%  
  filter(date >= as.Date("1868-01-01") & date <= as.Date("1883-01-01")) %>%  
  ggplot(mapping=aes(x = date, color = ville)) + geom_freqpoly()


prix_tot %>% group_by(ville) %>% summarise(m = mean(px_hecto_froment))


### wedding data




######### CONVERSION TO TIME SERIES

### monthly data

prix_mois = prix_tot %>% 
  mutate(date = ym(format(date, "%Y-%m"))) %>% 
  group_by(date) %>% 
  summarise(
    prix = mean(px_hecto_froment),
    .groups = 'drop') %>% 
  mutate(prix_lag9 = dplyr::lag(prix, n=9)) %>% 
  mutate(prix_lag15 = dplyr::lag(prix, n=15))

naissances_mois = registre %>% 
  mutate(date = ym(format(date, "%Y-%m"))) %>% 
  group_by(date) %>% 
  summarise(
    naissances = n(), 
    age_mere_moy = mean(age_mere, na.rm=TRUE),
    .groups = 'drop') %>% 
  mutate(freq_naissance = naissances/ days_in_month(date))


data_mois = left_join(naissances_mois, prix_mois, by=c("date"))  %>% mutate(month = format(date, "%m"))

write_csv(data_mois, "data/data_mois.csv")

### yearly data

prix_year = prix_tot %>% 
  mutate(year = format(date, "%Y")) %>% 
  group_by(year) %>% 
  summarise(prix = mean(px_hecto_froment)) %>% 
  mutate(prix_lag = dplyr::lag(prix, n=1))

naissances_year = registre %>% 
  mutate(year = format(date, "%Y")) %>% 
  group_by(year) %>% 
  summarise(naissances = n()) %>% 
  mutate(freq_naissance = naissances / ifelse(leap_year(as.numeric(year)), 366, 365))

data_year = left_join(naissances_year, prix_year, by="year")

write_csv(data_year, "data/data_year.csv")

