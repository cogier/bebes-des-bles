require("tidyverse")
require("jtools")
require("AER")
require("stargazer")
require("plm")
require("scales")
require("ggthemes")


registre <- read_csv("data/registre.csv")
prix_tot <- read_csv("data/mercuriales.csv")




bin <- 365.25 # used for aggregating the data and aligning the labels
rescale <- 6
offset <- 15

ggplot() + 
  geom_histogram(data = registre, mapping=aes(date, ..count..), binwidth = bin, boundary=as.Date("1869-01-01"), colour="white") +
  scale_x_date(
    breaks = seq(
      as.Date("1869-01-01"), 
      as.Date("1882-01-01"), 
      3*bin),
    labels = date_format("%Y-%b"),
    limits = c(as.Date("1867-01-01"), 
               as.Date("1884-01-01"))) + 
  geom_point(data = prix_tot, mapping = aes(x = date, y = rescale*(px_hecto_froment -offset), color=ville), shape=3, size=5) + 
  geom_smooth(data = prix_tot, mapping = aes(x = date, y = rescale*(px_hecto_froment-offset)), se = FALSE,method="lm") + 
  scale_y_continuous(
    # Features of the first axis
    name = "Naissances",
    # Add a second axis and specify its features
    sec.axis = sec_axis(~./rescale+offset, name="Prix du blé")) +
  theme_economist() +
  theme(axis.title.x=element_blank())

